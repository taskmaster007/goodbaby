­MODULE 1 : PYTHON BASICS:
		PRE-REQ: Basic syntax, https://github.com/kuleshov/cs228-material/blob/master/tutorials/python/cs228-python-tutorial.ipynb (nicely summarised,read whole) , https://learnxinyminutes.com/docs/python3/ (selective reading/simpliy to only what is required to implement in the codes )

0.1)Program to find pi decimal to Nth digit given by user

0.2)Binary to Decimal and Back Converter 

0.3)Change Return Prog­ram - The user enters a cost and then the amount of money given. The program will figure out the change and return it in the number of 1rs,2rs,5rs, 10rs, 20rs, 50rs whose combination takes lowest coins needed for the change

0.4)Count vowels from a txt file

0.5)Sort numbers within CSV file in ascending order and save in the file itself

0.6)Product Inventory Project - Create an application which manages an inventory of products. Create a product class which has a price, id, and quantity on hand. Then create an inventory class which keeps track of various products and can sum up the inventory value.

MODULE 2: OpenCV
		Prereq: https://www.pyimagesearch.com/2018/07/19/opencv-tutorial-a-guide-to-learn-opencv/, sentdex opencv(videos 1-5,10-15) https://www.youtube.com/watch?v=Z78zbnLlPUA&list=PLQVvvaa0QuDdttJXlLtAJxJetJcqmqlQq ,
			

0.9)Find objects in static image using countour detection and crop it.

MODULE 3: PYTHON+OPENCV REAL TIME.

		PREREQ: Module 1,2,  sentdex opencv(video 16,20,21) https://www.youtube.com/watch?v=Z78zbnLlPUA&list=PLQVvvaa0QuDdttJXlLtAJxJetJcqmqlQq
OpenCV documentation for haar cascasde, eigenfaces, LBPH, fisherfaces.

1)Create python code to detect face and show rectangle around face in output window using OpenCV

2)Create python code to align any detected face and show aligned face in separate window

3)Create python code to detect and recognise face and show name of person at top of rectangle in output window


MODULE 4: GUI using Tkinter
	Preq: Module 1,(20 min video)https://www.youtube.com/watch?v=_lSNIrR1nZU&t=655s , https://docs.python.org/2/library/tkinter.html(only required parts)

3.5)Create dialogue box with button for yes / no for coffee that gives respective messages in terminal upon clicking.
4)Create GUI for showing camera feed output using TKinter with ON/OFF buttons.



MODULE 5: GUI + Real time
	preq: Module 1,2,3,4.

5)Implement face recognition applictaion(code 3) using GUI in Tkinter.


MODULE 6: SESSION: OVERVIEW OF AI
	>Machine learning: general overview, what to study and what is impotant, major classifications, brief info of core topics.
	
MODULE 7: MACHINE LEARNING
	prereq:Module 1, https://github.com/adeshpande3/Machine-Learning-Links-And-Lessons-Learned#learning-machine-learning (follow =>learn machine learning=> till point 7) 
		For theory: Andrew Ng(31 attached ML videos, https://drive.google.com/drive/folders/1UWS2dKVSCXEfm7avs_gVVb2rTUxDoQsy  ), 
		For Practical: sentdex ML using python (*task for harshal to select videos*) https://www.youtube.com/watch?v=OGxgnH8y2NM&list=PLQVvvaa0QuDfKTOs3Keq_kaG2P55YRn5v
		Additional: >Siraj rawal ML course(videos 1-10 only) https://www.youtube.com/watch?v=uwwWVAgJBcM&list=PLOVs_uP0NWOc3QBJFHFcvH6tBKqC1Nhjr 
		>for complete ML summary in short(quick revision): https://www.commonlounge.com/discussion/c680313ec65a4503b92629859c287977 


6) Using  regression predict the stock of any company of your choice for 30 days.
7) Using classification models, take dataset of faces of humans and identify them as either male or female.
8)
8.5)*Update this module with further applications(task to darpan)*

MODULE 8: SESSION: OVERVIEW OF AI 2
>Deep learning in breif, why is it required. Small explanation of ANN, CNN, GNN, RNN. What is a model, what is training a model, why tensorflow, how TF works,frozen graph etc.

MODULE 9: DEEP  LEARNING
	PREREQ: Module 1,2,3,7,https://github.com/adeshpande3/Machine-Learning-Links-And-Lessons-Learned#learning-machine-learning (follow =>learn machine learning=> points 8-11) 
		For theory: Andrew Ng(14 DL videos, https://drive.google.com/drive/folders/1UWS2dKVSCXEfm7avs_gVVb2rTUxDoQsy )
		For practical: Sentdex Tensorflow/DL:(only 1-4,9-14 videos)  https://www.youtube.com/watch?v=oYbVFhK_olY&list=PLSPWNkAMSvv5DKeSVDbEbUKSsK4Z-GgiP 
		additional: >for complete deep learning summary(quick revision): https://www.commonlounge.com/discussion/8d5612f5517944059a61f92aa8b86770  
		>Siraj rawal DL playlist(only Intro to deep learning series #15 videos[10 mins each], no need to watch live videos): https://www.youtube.com/watch?v=vOppzHpvTiQ&list=PL2-dafEMk2A7YdKv4XfKpfbTH5z6rEEj3 

10)Use deep-learning methods to implement facial recognition (ex. FaceNet, Inception models)

11)Recognition of characters (a,b,c,d...) from images

12)Using keras implement image classification on you own image dataset for detecing a unknown image related to dataset by creating your own model.




MODULE 10: SOUND (OR)
PREREQ: Module 9
13)Use deep-learning to recognise sound (not NLP, just recognise what single word was spoken)
14) Starting with NLP

MODULE 10:ADVANCED COMPUTER VISION
PREREQ: Module 9
15)Real time person postion detection using deep leaning models
16)Real time weapon detection, or any object that the model is pre-trained upon.


							




								Documented by:  Shreeyash and Harshal


